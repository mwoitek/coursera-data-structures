package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	root := getRootNode()
	printInOrder(root)
	fmt.Print("\n")
	printPreOrder(root)
	fmt.Print("\n")
	printPostOrder(root)
	fmt.Print("\n")
}

// These functions are absolutely necessary! Do not use the
// functions in the "fmt" package for reading input! They are
// painfully slow. It's not possible to pass this assignment if
// you use them!

func readInt(reader *bufio.Reader) int {
	line, _ := reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")
	num, _ := strconv.Atoi(line)
	return num
}

func readInts(reader *bufio.Reader) (int, int, int) {
	line, _ := reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")
	nums := strings.Fields(line)
	num1, _ := strconv.Atoi(nums[0])
	num2, _ := strconv.Atoi(nums[1])
	num3, _ := strconv.Atoi(nums[2])
	return num1, num2, num3
}

type node struct {
	key         int
	left, right *node
}

func getRootNode() *node {
	reader := bufio.NewReader(os.Stdin)
	numNodes := readInt(reader)
	nodes := make([]node, numNodes)
	var key, left, right int
	for i := range nodes {
		key, left, right = readInts(reader)
		nodes[i].key = key
		if left != -1 {
			nodes[i].left = &nodes[left]
		}
		if right != -1 {
			nodes[i].right = &nodes[right]
		}
	}
	return &nodes[0]
}

func printInOrder(currNode *node) {
	if currNode == nil {
		return
	}
	printInOrder(currNode.left)
	fmt.Printf("%d ", currNode.key)
	printInOrder(currNode.right)
}

func printPreOrder(currNode *node) {
	if currNode == nil {
		return
	}
	fmt.Printf("%d ", currNode.key)
	printPreOrder(currNode.left)
	printPreOrder(currNode.right)
}

func printPostOrder(currNode *node) {
	if currNode == nil {
		return
	}
	printPostOrder(currNode.left)
	printPostOrder(currNode.right)
	fmt.Printf("%d ", currNode.key)
}
