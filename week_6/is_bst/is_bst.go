package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	inputData := readInput()
	t := tree{}
	t.build(inputData)
	if t.isBST() {
		fmt.Println("CORRECT")
	} else {
		fmt.Println("INCORRECT")
	}
}

// Reading input

type inputLine struct {
	key, left, right int
}

func processLine(line string) inputLine {
	line = strings.TrimSuffix(line, "\n")
	lineParts := strings.Fields(line)
	key, _ := strconv.Atoi(lineParts[0])
	left, _ := strconv.Atoi(lineParts[1])
	right, _ := strconv.Atoi(lineParts[2])
	return inputLine{key: key, left: left, right: right}
}

func readInput() []inputLine {
	reader := bufio.NewReader(os.Stdin)
	var line string
	line, _ = reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")
	numNodes, _ := strconv.Atoi(line)
	inputData := make([]inputLine, numNodes)
	if numNodes == 0 {
		return inputData
	}
	for i := range inputData {
		line, _ = reader.ReadString('\n')
		inputData[i] = processLine(line)
	}
	return inputData
}

// Binary tree implementation

type node struct {
	key         int
	left, right *node
}

type tree struct {
	root     *node
	numNodes int
}

func (t *tree) build(inputData []inputLine) {
	t.numNodes = len(inputData)
	if t.numNodes == 0 {
		return
	}
	nodes := make([]node, t.numNodes)
	for i, input := range inputData {
		nodes[i].key = input.key
		if input.left != -1 {
			nodes[i].left = &nodes[input.left]
		}
		if input.right != -1 {
			nodes[i].right = &nodes[input.right]
		}
	}
	t.root = &nodes[0]
}

func inOrderTraversal(currNode *node, keys *[]int) {
	if currNode == nil {
		return
	}
	inOrderTraversal(currNode.left, keys)
	*keys = append(*keys, currNode.key)
	inOrderTraversal(currNode.right, keys)
}

func (t *tree) isBST() bool {
	keys := make([]int, 0, t.numNodes)
	inOrderTraversal(t.root, &keys)
	return sort.SliceIsSorted(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
}
