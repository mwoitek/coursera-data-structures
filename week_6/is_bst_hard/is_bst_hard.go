package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const (
	MaxUint = ^uint(0)
	MaxInt  = int(MaxUint >> 1)
	MinInt  = -MaxInt - 1
)

func main() {
	inputData := readInput()
	t := tree{}
	t.build(inputData)
	if t.isBST() {
		fmt.Println("CORRECT")
	} else {
		fmt.Println("INCORRECT")
	}
}

// Reading input

type inputLine struct {
	key, left, right int
}

func processLine(line string) inputLine {
	line = strings.TrimSuffix(line, "\n")
	lineParts := strings.Fields(line)
	key, _ := strconv.Atoi(lineParts[0])
	left, _ := strconv.Atoi(lineParts[1])
	right, _ := strconv.Atoi(lineParts[2])
	return inputLine{key, left, right}
}

func readInput() []inputLine {
	reader := bufio.NewReader(os.Stdin)
	var line string
	line, _ = reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")
	numNodes, _ := strconv.Atoi(line)
	inputData := make([]inputLine, numNodes)
	if numNodes == 0 {
		return inputData
	}
	for i := range inputData {
		line, _ = reader.ReadString('\n')
		inputData[i] = processLine(line)
	}
	return inputData
}

// Binary tree implementation

type node struct {
	key         int
	left, right *node
}

type tree struct {
	root *node
}

func (t *tree) build(inputData []inputLine) {
	numNodes := len(inputData)
	if numNodes == 0 {
		return
	}
	nodes := make([]node, numNodes)
	for i, input := range inputData {
		nodes[i].key = input.key
		if input.left != -1 {
			nodes[i].left = &nodes[input.left]
		}
		if input.right != -1 {
			nodes[i].right = &nodes[input.right]
		}
	}
	t.root = &nodes[0]
}

func validate(currNode *node, minLimit, maxLimit int) bool {
	if currNode == nil {
		return true
	}
	return minLimit <= currNode.key &&
		currNode.key <= maxLimit &&
		validate(currNode.left, minLimit, currNode.key-1) &&
		validate(currNode.right, currNode.key, maxLimit)
}

func (t *tree) isBST() bool {
	return validate(t.root, MinInt, MaxInt)
}
