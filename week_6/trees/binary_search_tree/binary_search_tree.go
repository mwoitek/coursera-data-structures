// This is not one of the assignments. However, I cannot implement
// a splay tree without first coding the "simpler" tree structures.

package main

import (
	"fmt"
	"strconv"
	"strings"
)

type treeNode struct {
	data       int
	parent     *treeNode
	leftChild  *treeNode
	rightChild *treeNode
}

type binarySearchTree struct {
	root *treeNode
}

func (bst binarySearchTree) isEmpty() bool {
	return bst.root == nil
}

func (bst *binarySearchTree) insert(data int) {
	currNode := bst.root
	var parent *treeNode = nil
	for currNode != nil {
		parent = currNode
		if data < currNode.data {
			currNode = currNode.leftChild
		} else {
			currNode = currNode.rightChild
		}
	}
	newNode := &treeNode{data: data, parent: parent}
	if parent == nil {
		bst.root = newNode
	} else if data < parent.data {
		parent.leftChild = newNode
	} else {
		parent.rightChild = newNode
	}
}

func _find(currNode *treeNode, data int) *treeNode {
	if currNode == nil {
		return nil
	}
	if data == currNode.data {
		return currNode
	}
	if data < currNode.data {
		return _find(currNode.leftChild, data)
	} else {
		return _find(currNode.rightChild, data)
	}
}

func (bst binarySearchTree) contains(data int) bool {
	return _find(bst.root, data) != nil
}

func (bst binarySearchTree) find(data int) *treeNode {
	return _find(bst.root, data)
}

func (bst binarySearchTree) max(startNode *treeNode) *treeNode {
	currNode := startNode
	for currNode.rightChild != nil {
		currNode = currNode.rightChild
	}
	return currNode
}

func (bst binarySearchTree) min(startNode *treeNode) *treeNode {
	currNode := startNode
	for currNode.leftChild != nil {
		currNode = currNode.leftChild
	}
	return currNode
}

func (bst binarySearchTree) predecessor(node *treeNode) *treeNode {
	if node.leftChild != nil {
		return bst.max(node.leftChild)
	}
	currNode := node
	parent := node.parent
	for parent != nil && currNode == parent.leftChild {
		currNode = parent
		parent = parent.parent
	}
	return parent
}

func (bst binarySearchTree) successor(node *treeNode) *treeNode {
	if node.rightChild != nil {
		return bst.min(node.rightChild)
	}
	currNode := node
	parent := node.parent
	for parent != nil && currNode == parent.rightChild {
		currNode = parent
		parent = parent.parent
	}
	return parent
}

func (bst *binarySearchTree) shiftNodes(node1, node2 *treeNode) {
	if node1.parent == nil {
		bst.root = node2
	} else if node1 == node1.parent.leftChild {
		node1.parent.leftChild = node2
	} else {
		node1.parent.rightChild = node2
	}
	if node2 != nil {
		node2.parent = node1.parent
	}
}

func (bst *binarySearchTree) delete(data int) {
	nodeToDelete := bst.find(data)
	if nodeToDelete == nil {
		return
	}
	if nodeToDelete.leftChild == nil {
		bst.shiftNodes(nodeToDelete, nodeToDelete.rightChild)
	} else if nodeToDelete.rightChild == nil {
		bst.shiftNodes(nodeToDelete, nodeToDelete.leftChild)
	} else {
		successor := bst.successor(nodeToDelete)
		if successor.parent != nodeToDelete {
			bst.shiftNodes(successor, successor.rightChild)
			successor.rightChild = nodeToDelete.rightChild
			successor.rightChild.parent = successor
		}
		bst.shiftNodes(nodeToDelete, successor)
		successor.leftChild = nodeToDelete.leftChild
		successor.leftChild.parent = successor
	}
}

func (bst binarySearchTree) printInOrder() {
	var builder strings.Builder
	var dataStr string

	var inOrderTraversal func(*treeNode)
	inOrderTraversal = func(currNode *treeNode) {
		if currNode == nil {
			return
		}
		inOrderTraversal(currNode.leftChild)
		dataStr = strconv.Itoa(currNode.data)
		builder.WriteString(dataStr + " ")
		inOrderTraversal(currNode.rightChild)
	}

	inOrderTraversal(bst.root)
	out := strings.TrimSuffix(builder.String(), " ")
	fmt.Println(out)
}

func main() {
	bst := binarySearchTree{}
	fmt.Printf("Is tree empty? %t\n", bst.isEmpty())

	fmt.Println("Inserting tree nodes...")
	ints := [13]int{7, 5, 12, 9, 6, 15, 3, 1, 8, 4, 10, 13, 17}
	for _, i := range ints {
		bst.insert(i)
	}
	fmt.Printf("Is tree empty? %t\n", bst.isEmpty())

	fmt.Printf("Contains %d? %t\n", 2, bst.contains(2))
	fmt.Printf("Contains %d? %t\n", 6, bst.contains(6))

	fmt.Printf("Min value: %d\n", bst.min(bst.root).data)
	fmt.Printf("Max value: %d\n", bst.max(bst.root).data)

	val := 12
	node := bst.find(val)
	fmt.Printf("Min value in the sub-tree rooted at %d: %d\n", val, bst.min(node).data)

	val = 5
	node = bst.find(val)
	fmt.Printf("Max value in the sub-tree rooted at %d: %d\n", val, bst.max(node).data)

	bst.printInOrder()

	val = 4
	fmt.Printf("Deleting value %d...\n", val)
	bst.delete(val)
	bst.printInOrder()

	val = 5
	fmt.Printf("Deleting value %d...\n", val)
	bst.delete(val)
	bst.printInOrder()

	val = 15
	fmt.Printf("Deleting value %d...\n", val)
	bst.delete(val)
	bst.printInOrder()

	val = 7
	fmt.Printf("Deleting value %d...\n", val)
	bst.delete(val)
	bst.printInOrder()

	val = 2
	fmt.Printf("Deleting value %d...\n", val)
	bst.delete(val)
	bst.printInOrder()
}
