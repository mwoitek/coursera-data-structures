package main

import (
	"fmt"
	"strconv"
	"strings"
)

type treeNode struct {
	data   int
	left   *treeNode
	right  *treeNode
	height int
}

type avlTree struct {
	root *treeNode
}

// Read-only operations for AVL trees are the same as for BSTs!

func (t avlTree) isEmpty() bool {
	return t.root == nil
}

func _find(currNode *treeNode, data int) *treeNode {
	if currNode == nil {
		return nil
	}
	if data == currNode.data {
		return currNode
	}
	if data < currNode.data {
		return _find(currNode.left, data)
	} else {
		return _find(currNode.right, data)
	}
}

func (t avlTree) contains(data int) bool {
	return _find(t.root, data) != nil
}

func (t avlTree) find(data int) *treeNode {
	return _find(t.root, data)
}

func maxNode(startNode *treeNode) *treeNode {
	node := startNode
	for node.right != nil {
		node = node.right
	}
	return node
}

func (t avlTree) printInOrder() {
	var builder strings.Builder
	var dataStr string

	var inOrderTraversal func(*treeNode)
	inOrderTraversal = func(currNode *treeNode) {
		if currNode == nil {
			return
		}
		inOrderTraversal(currNode.left)
		dataStr = strconv.Itoa(currNode.data)
		builder.WriteString(dataStr + " ")
		inOrderTraversal(currNode.right)
	}

	inOrderTraversal(t.root)
	out := strings.TrimSuffix(builder.String(), " ")
	fmt.Println(out)
}

// Here comes the code that is different...

func getHeight(node *treeNode) int {
	if node == nil {
		return 0
	}
	return node.height
}

func getBalance(node *treeNode) int {
	if node == nil {
		return 0
	}
	return getHeight(node.left) - getHeight(node.right)
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func updateHeight(node *treeNode) {
	maxHeight := max(getHeight(node.left), getHeight(node.right))
	node.height = maxHeight + 1
}

func rotateLeft(node *treeNode) *treeNode {
	rightNode := node.right
	centerNode := rightNode.left
	rightNode.left = node
	node.right = centerNode
	updateHeight(node)
	updateHeight(rightNode)
	return rightNode
}

func rotateRight(node *treeNode) *treeNode {
	leftNode := node.left
	centerNode := leftNode.right
	leftNode.right = node
	node.left = centerNode
	updateHeight(node)
	updateHeight(leftNode)
	return leftNode
}

func rotate(node *treeNode) *treeNode {
	balance := getBalance(node)
	if balance > 1 { // Left-heavy
		if getBalance(node.left) < 0 {
			node.left = rotateLeft(node.left)
		}
		return rotateRight(node)
	}
	if balance < -1 { // Right-heavy
		if getBalance(node.right) > 0 {
			node.right = rotateRight(node.right)
		}
		return rotateLeft(node)
	}
	return node
}

func _insert(node *treeNode, data int) *treeNode {
	if node == nil {
		return &treeNode{data: data, height: 1}
	}
	if data < node.data {
		node.left = _insert(node.left, data)
	} else if data > node.data {
		node.right = _insert(node.right, data)
	} else {
		return node // No duplicate value
	}
	updateHeight(node)
	return rotate(node)
}

func (t *avlTree) insert(data int) {
	t.root = _insert(t.root, data)
}

func _delete(node *treeNode, data int) *treeNode {
	if node == nil {
		return nil
	}
	if data < node.data {
		node.left = _delete(node.left, data)
	} else if data > node.data {
		node.right = _delete(node.right, data)
	} else {
		if node.left == nil {
			return node.right
		}
		if node.right == nil {
			return node.left
		}
		predecessor := maxNode(node.left)
		node.data = predecessor.data
		node.left = _delete(node.left, node.data)
	}
	updateHeight(node)
	return rotate(node)
}

func (t *avlTree) delete(data int) {
	t.root = _delete(t.root, data)
}

// TODO Implement merge and split

func main() {
	tree := avlTree{}

	ints := [13]int{7, 5, 12, 9, 6, 15, 3, 1, 8, 4, 10, 13, 17}
	for _, i := range ints {
		tree.insert(i)
	}

	tree.printInOrder()

	valsToDelete := [5]int{4, 5, 15, 7, 2}
	for _, val := range valsToDelete {
		fmt.Printf("Deleting value %d...\n", val)
		tree.delete(val)
		tree.printInOrder()
	}
}
