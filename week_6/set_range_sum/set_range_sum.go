package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const (
	infinity int = int(^uint(0) >> 1)
	m        int = 1000000001
)

// Splay tree implementation

type treeNode struct {
	data       int
	parent     *treeNode
	left       *treeNode
	right      *treeNode
	subtreeSum int
}

type splayTree struct {
	root *treeNode
}

// Subtree sum

func getSubtreeSum(node *treeNode) int {
	if node == nil {
		return 0
	}
	return node.subtreeSum
}

func (node *treeNode) updateSubtreeSum() {
	node.subtreeSum = node.data
	node.subtreeSum += getSubtreeSum(node.left)
	node.subtreeSum += getSubtreeSum(node.right)
}

// Splay operation

func (node *treeNode) isLeftChild() bool {
	if node.parent == nil {
		return false
	}
	return node == node.parent.left
}

func (node *treeNode) isRightChild() bool {
	if node.parent == nil {
		return false
	}
	return node == node.parent.right
}

func (t *splayTree) rotateLeft(node *treeNode) {
	rightNode := node.right
	if rightNode != nil {
		node.right = rightNode.left
		node.updateSubtreeSum()
		if node.right != nil {
			node.right.parent = node
		}
		rightNode.parent = node.parent
	}
	if node.parent == nil {
		t.root = rightNode
	} else if node.isLeftChild() {
		node.parent.left = rightNode
	} else {
		node.parent.right = rightNode
	}
	if rightNode != nil {
		rightNode.left = node
		rightNode.updateSubtreeSum()
	}
	node.parent = rightNode
}

func (t *splayTree) rotateRight(node *treeNode) {
	leftNode := node.left
	if leftNode != nil {
		node.left = leftNode.right
		node.updateSubtreeSum()
		if node.left != nil {
			node.left.parent = node
		}
		leftNode.parent = node.parent
	}
	if node.parent == nil {
		t.root = leftNode
	} else if node.isLeftChild() {
		node.parent.left = leftNode
	} else {
		node.parent.right = leftNode
	}
	if leftNode != nil {
		leftNode.right = node
		leftNode.updateSubtreeSum()
	}
	node.parent = leftNode
}

func (node *treeNode) getGrandparent() *treeNode {
	if node.parent == nil {
		return nil
	}
	return node.parent.parent
}

func (t *splayTree) splay(node *treeNode) {
	for node != t.root {
		if node.getGrandparent() == nil {
			if node.isLeftChild() {
				t.rotateRight(node.parent)
			} else {
				t.rotateLeft(node.parent)
			}
		} else if node.isLeftChild() && node.parent.isLeftChild() {
			t.rotateRight(node.getGrandparent())
			t.rotateRight(node.parent)
		} else if node.isRightChild() && node.parent.isRightChild() {
			t.rotateLeft(node.getGrandparent())
			t.rotateLeft(node.parent)
		} else if node.isLeftChild() && node.parent.isRightChild() {
			t.rotateRight(node.parent)
			t.rotateLeft(node.parent)
		} else {
			t.rotateLeft(node.parent)
			t.rotateRight(node.parent)
		}
	}
}

// Insert operation

func (t *splayTree) insert(data int) {
	node := t.root
	var parent *treeNode = nil
	for node != nil {
		if data == node.data {
			t.splay(node)
			return
		}
		parent = node
		if data < node.data {
			node = node.left
		} else {
			node = node.right
		}
	}
	newNode := &treeNode{data: data, parent: parent, subtreeSum: data}
	if parent == nil {
		t.root = newNode
	} else if data < parent.data {
		parent.left = newNode
		parent.updateSubtreeSum()
	} else {
		parent.right = newNode
		parent.updateSubtreeSum()
	}
	t.splay(newNode)
}

// Find operation

func (t *splayTree) find(data int) *treeNode {
	if t.root == nil {
		return nil
	}
	node := t.root
	for data != node.data {
		if data < node.data {
			if node.left == nil {
				break
			}
			node = node.left
		} else {
			if node.right == nil {
				break
			}
			node = node.right
		}
	}
	t.splay(node)
	return node
}

func (t *splayTree) contains(data int) bool {
	node := t.find(data)
	return node != nil && data == node.data
}

// Merge operation

func merge(leftTree, rightTree splayTree) splayTree {
	if leftTree.root == nil {
		return rightTree
	}
	node := leftTree.find(infinity)
	node.right = rightTree.root
	node.updateSubtreeSum()
	if rightTree.root != nil {
		rightTree.root.parent = node
	}
	return leftTree
}

// Delete operation

func (t *splayTree) delete(data int) {
	nodeToDel := t.find(data)
	if nodeToDel == nil || data != nodeToDel.data {
		return
	}
	leftRoot := nodeToDel.left
	if leftRoot != nil {
		nodeToDel.left = nil
		leftRoot.parent = nil
	}
	rightRoot := nodeToDel.right
	if rightRoot != nil {
		nodeToDel.right = nil
		rightRoot.parent = nil
	}
	merged := merge(splayTree{leftRoot}, splayTree{rightRoot})
	t.root = merged.root
}

// Split operation

func split(tree splayTree, data int) (splayTree, splayTree) {
	node := tree.find(data)
	if node == nil {
		return splayTree{}, splayTree{}
	}
	if node.data <= data {
		rightRoot := node.right
		if rightRoot != nil {
			node.right = nil
			node.updateSubtreeSum()
			rightRoot.parent = nil
		}
		return tree, splayTree{rightRoot}
	} else {
		leftRoot := node.left
		if leftRoot != nil {
			node.left = nil
			node.updateSubtreeSum()
			leftRoot.parent = nil
		}
		return splayTree{leftRoot}, tree
	}
}

// Range sum

func (t *splayTree) rangeSum(x, y int) int {
	if t.root == nil {
		return 0
	}
	leftTree, rightTree := split(*t, y)
	sum := getSubtreeSum(leftTree.root)
	merged := merge(leftTree, rightTree)
	t.root = merged.root
	leftTree, rightTree = split(*t, x)
	sum -= getSubtreeSum(leftTree.root)
	merged = merge(leftTree, rightTree)
	t.root = merged.root
	node := t.find(x)
	if node.data == x {
		sum += x
	}
	return sum
}

// Solution to the problem

func readInput() []string {
	reader := bufio.NewReader(os.Stdin)
	line, _ := reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")
	numQueries, _ := strconv.Atoi(line)
	queries := make([]string, numQueries)
	for i := range queries {
		line, _ = reader.ReadString('\n')
		line = strings.TrimSuffix(line, "\n")
		queries[i] = line
	}
	return queries
}

type intSet struct {
	tree splayTree
	x    int
}

func (s *intSet) add(i int) {
	s.tree.insert((i + s.x) % m)
}

func (s *intSet) del(i int) {
	s.tree.delete((i + s.x) % m)
}

func (s *intSet) find(i int) {
	if s.tree.contains((i + s.x) % m) {
		fmt.Println("Found")
	} else {
		fmt.Println("Not found")
	}
}

func (s *intSet) sum(l, r int) {
	s.x = s.tree.rangeSum((l+s.x)%m, (r+s.x)%m)
	fmt.Println(s.x)
	s.x %= m
}

func (s *intSet) runQuery(query string) {
	queryParts := strings.Fields(query)
	param1, _ := strconv.Atoi(queryParts[1])
	switch queryParts[0] {
	case "+":
		s.add(param1)
	case "-":
		s.del(param1)
	case "?":
		s.find(param1)
	case "s":
		param2, _ := strconv.Atoi(queryParts[2])
		s.sum(param1, param2)
	}
}

func main() {
	queries := readInput()
	s := intSet{}
	for _, query := range queries {
		s.runQuery(query)
	}
}
