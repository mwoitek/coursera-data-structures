import heapq
from collections import deque
from typing import Deque, List, NamedTuple


def main():
    num_threads = next(map(int, input().split(" ")))
    jobs_process_times = read_process_times()

    assigned_jobs = assign_jobs(num_threads, jobs_process_times)
    print_assigned_jobs(assigned_jobs)


class AssignedJob(NamedTuple):
    thread_idx: int
    start_time: int

    def __str__(self):
        return f"{self.thread_idx} {self.start_time}"


class ThreadsPQEntry(NamedTuple):
    next_free_time: int
    thread_idx: int


def read_process_times() -> Deque[int]:
    return deque(map(int, input().split(" ")))


def assign_jobs(num_threads: int, jobs_process_times: Deque[int]) -> Deque[AssignedJob]:
    assigned_jobs: Deque[AssignedJob] = deque([], len(jobs_process_times))
    threads_pq: List[ThreadsPQEntry] = []

    thread_idx = 0
    while len(jobs_process_times) > 0 and thread_idx < num_threads:
        assigned_jobs.append(AssignedJob(thread_idx, start_time=0))
        next_free_time = jobs_process_times.popleft()
        heapq.heappush(threads_pq, ThreadsPQEntry(next_free_time, thread_idx))
        thread_idx += 1

    while len(jobs_process_times) > 0:
        next_free_time, thread_idx = heapq.heappop(threads_pq)
        assigned_jobs.append(AssignedJob(thread_idx, start_time=next_free_time))
        next_free_time += jobs_process_times.popleft()
        heapq.heappush(threads_pq, ThreadsPQEntry(next_free_time, thread_idx))

    return assigned_jobs


def print_assigned_jobs(assigned_jobs: Deque[AssignedJob]) -> None:
    for assigned_job in assigned_jobs:
        print(assigned_job)


if __name__ == "__main__":
    main()
