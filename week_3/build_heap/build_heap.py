from collections import deque
from math import floor
from typing import Any, Deque, List, Tuple

Pair = Tuple[int, int]
DequePairs = Deque[Pair]


def main():
    # pylint: disable=unused-variable
    n = int(input())  # Useless but I have to read it
    arr = list(map(int, input().split(" ")))

    idx_swapped = build_heap(arr)
    print(len(idx_swapped))
    print_idx_swapped(idx_swapped)


def left_child(node_idx: int) -> int:
    return 2 * node_idx + 1


def right_child(node_idx: int) -> int:
    return 2 * (node_idx + 1)


def sift_down(arr: List[Any], node_idx: int) -> DequePairs:
    idx_swapped: DequePairs = deque([], 4 * len(arr))

    def _sift_down(node_idx: int) -> None:
        min_idx = node_idx

        left_idx = left_child(node_idx)
        if left_idx < len(arr) and arr[left_idx] < arr[min_idx]:
            min_idx = left_idx

        right_idx = right_child(node_idx)
        if right_idx < len(arr) and arr[right_idx] < arr[min_idx]:
            min_idx = right_idx

        if min_idx != node_idx:
            arr[node_idx], arr[min_idx] = arr[min_idx], arr[node_idx]
            idx_swapped.append((node_idx, min_idx))
            _sift_down(min_idx)

    _sift_down(node_idx)
    return idx_swapped


def build_heap(arr: List[Any]) -> DequePairs:
    size = len(arr)
    idx_swapped: DequePairs = deque([], 4 * size)
    start_idx = floor(size / 2) - 1
    for node_idx in range(start_idx, -1, -1):
        idx_swapped.extend(sift_down(arr, node_idx))
    return idx_swapped


def print_idx_swapped(idx_swapped: DequePairs) -> None:
    for idx_1, idx_2 in idx_swapped:
        print(f"{idx_1} {idx_2}")


if __name__ == "__main__":
    main()
