from math import prod


class DisjointSets:
    def __init__(self, size: int) -> None:
        self.size = size
        self.parents = [-1] * size
        self.ranks = [-1] * size

    def make_set(self, i: int) -> None:
        self.parents[i - 1] = i
        self.ranks[i - 1] = 0

    def find(self, i: int) -> int:
        while i != self.parents[i - 1]:
            i = self.parents[i - 1]
        return i

    def union(self, i: int, j: int) -> None:
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        if self.ranks[i_id - 1] > self.ranks[j_id - 1]:
            self.parents[j_id - 1] = i_id
        else:
            self.parents[i_id - 1] = j_id
            if self.ranks[i_id - 1] == self.ranks[j_id - 1]:
                self.ranks[j_id - 1] += 1

    def product_of_heights(self) -> int:
        root_idxs = filter(lambda i: self.parents[i] == i + 1, range(self.size))
        heights = map(lambda i: self.ranks[i], root_idxs)
        return prod(heights)


if __name__ == "__main__":
    size = 12
    ds = DisjointSets(size)
    for i in range(1, size + 1):
        ds.make_set(i)
    ds.union(2, 10)
    ds.union(7, 5)
    ds.union(6, 1)
    ds.union(3, 4)
    ds.union(5, 11)
    ds.union(7, 8)
    ds.union(7, 3)
    ds.union(12, 2)
    ds.union(9, 6)
    print(f"Product of heights: {ds.product_of_heights()}")  # Product of heights: 2
