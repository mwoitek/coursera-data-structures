# Naive implementation of disjoint sets
class DisjointSets:
    def __init__(self, size: int) -> None:
        self.size = size
        self.smallest = [0] * size

    def make_set(self, i: int) -> None:
        self.smallest[i - 1] = i

    def find(self, i: int) -> int:
        return self.smallest[i - 1]

    def union(self, i: int, j: int) -> None:
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        m = min(i_id, j_id)
        for k in range(self.size):
            if self.smallest[k] == i_id or self.smallest[k] == j_id:
                self.smallest[k] = m


if __name__ == "__main__":
    size = 12
    ds = DisjointSets(size)
    for i in range(1, size + 1):
        ds.make_set(i)
    ds.union(2, 10)
    ds.union(7, 5)
    ds.union(6, 1)
    ds.union(3, 4)
    ds.union(5, 11)
    ds.union(7, 8)
    ds.union(7, 3)
    ds.union(12, 2)
    ds.union(9, 6)
    ids = map(lambda i: ds.find(i), [6, 3, 11, 9])
    print(" ".join(map(str, ids)))  # 1 3 3 1
