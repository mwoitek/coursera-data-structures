class DisjointSets:
    def __init__(self, size: int) -> None:
        self.size = size
        self.parents = [-1] * size
        self.ranks = [-1] * size

    def make_set(self, i: int) -> None:
        self.parents[i - 1] = i
        self.ranks[i - 1] = 0

    def find(self, i: int) -> int:
        if i != self.parents[i - 1]:
            self.parents[i - 1] = self.find(self.parents[i - 1])
        return self.parents[i - 1]

    def union(self, i: int, j: int) -> None:
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        if self.ranks[i_id - 1] > self.ranks[j_id - 1]:
            self.parents[j_id - 1] = i_id
        else:
            self.parents[i_id - 1] = j_id
            if self.ranks[i_id - 1] == self.ranks[j_id - 1]:
                self.ranks[j_id - 1] += 1

    def subtree_height(self, idx: int) -> int:
        height = 0
        while self.parents[idx] != idx + 1:
            height += 1
            idx = self.parents[idx] - 1
        return height

    def max_tree_height(self) -> int:
        heights = map(lambda idx: self.subtree_height(idx), range(self.size))
        return max(heights)


if __name__ == "__main__":
    ds = DisjointSets(60)
    for i in range(1, 61):
        ds.make_set(i)
    for i in range(1, 31):
        ds.union(i, 2 * i)
    for i in range(1, 21):
        ds.union(i, 3 * i)
    for i in range(1, 13):
        ds.union(i, 5 * i)
    for i in range(1, 61):
        ds.find(i)
    print(f"Maximum tree height: {ds.max_tree_height()}")  # Maximum tree height: 1
