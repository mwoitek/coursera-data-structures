from sys import argv


class DisjointSets:
    def __init__(self, size: int) -> None:
        self.size = size
        self.parents = [-1] * size
        self.ranks = [-1] * size

    def make_set(self, i: int) -> None:
        self.parents[i - 1] = i
        self.ranks[i - 1] = 0

    def find(self, i: int) -> int:
        while i != self.parents[i - 1]:
            i = self.parents[i - 1]
        return i

    def union(self, i: int, j: int) -> None:
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        if self.ranks[i_id - 1] > self.ranks[j_id - 1]:
            self.parents[j_id - 1] = i_id
        else:
            self.parents[i_id - 1] = j_id
            if self.ranks[i_id - 1] == self.ranks[j_id - 1]:
                self.ranks[j_id - 1] += 1

    def number_of_trees(self) -> int:
        return sum(map(lambda i: self.parents[i] == i + 1, range(self.size)))

    def max_tree_height(self) -> int:
        root_idxs = filter(lambda i: self.parents[i] == i + 1, range(self.size))
        heights = map(lambda i: self.ranks[i], root_idxs)
        return max(heights)


if __name__ == "__main__":
    n = int(argv[1])
    ds = DisjointSets(n)
    for i in range(1, n + 1):
        ds.make_set(i)
    for i in range(1, n):
        ds.union(i, i + 1)
    print(f"n = {n}")
    print(f"Number of trees = {ds.number_of_trees()}")
    print(f"Maximum tree height = {ds.max_tree_height()}")
    # Answer: One tree of height 1.
