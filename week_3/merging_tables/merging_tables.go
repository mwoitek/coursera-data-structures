package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Disjoint sets implementation

type disjointSets struct {
	parents, ranks []int
}

func newDisjointSets(size int) disjointSets {
	parents := make([]int, size)
	ranks := make([]int, size)
	for i := 0; i < size; i++ {
		parents[i] = -1
		ranks[i] = -1
	}
	return disjointSets{parents, ranks}
}

func (ds *disjointSets) makeSet(i int) {
	ds.parents[i-1] = i
	ds.ranks[i-1] = 0
}

func (ds *disjointSets) find(i int) int {
	if i != ds.parents[i-1] {
		ds.parents[i-1] = ds.find(ds.parents[i-1])
	}
	return ds.parents[i-1]
}

func (ds *disjointSets) union(i, j int) {
	iId := ds.find(i)
	jId := ds.find(j)
	if iId == jId {
		return
	}
	if ds.ranks[iId-1] > ds.ranks[jId-1] {
		ds.parents[jId-1] = iId
	} else {
		ds.parents[iId-1] = jId
		if ds.ranks[iId-1] == ds.ranks[jId-1] {
			ds.ranks[jId-1] += 1
		}
	}
}

// Solution to the problem

type table struct {
	numRows, symlink int
}

func newTable(numRows int) table {
	return table{numRows, -1}
}

type mergeQuery struct {
	destination, source int
}

func newMergeQuery(line string) mergeQuery {
	lineParts := strings.Fields(line)
	destination, _ := strconv.Atoi(lineParts[0])
	source, _ := strconv.Atoi(lineParts[1])
	return mergeQuery{destination, source}
}

func readInput() ([]table, int, []mergeQuery) {
	reader := bufio.NewReader(os.Stdin)

	line, _ := reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")

	lineParts := strings.Fields(line)
	numTables, _ := strconv.Atoi(lineParts[0])
	numQueries, _ := strconv.Atoi(lineParts[1])
	tables := make([]table, numTables)
	queries := make([]mergeQuery, numQueries)

	line, _ = reader.ReadString('\n')
	line = strings.TrimSuffix(line, "\n")

	lineParts = strings.Fields(line)
	var numRows int
	maxNumRows := -1
	for i, numRowsStr := range lineParts {
		numRows, _ = strconv.Atoi(numRowsStr)
		tables[i] = newTable(numRows)
		if numRows > maxNumRows {
			maxNumRows = numRows
		}
	}

	for i := range queries {
		line, _ = reader.ReadString('\n')
		line = strings.TrimSuffix(line, "\n")
		queries[i] = newMergeQuery(line)
	}

	return tables, maxNumRows, queries
}

type tableMerger struct {
	tables     []table
	maxNumRows int
	ds         disjointSets
	trueIdMap  map[int]int
}

func newTableMerger(tables []table, maxNumRows int) tableMerger {
	numTables := len(tables)
	ds := newDisjointSets(numTables)
	trueIdMap := make(map[int]int)
	for i := 1; i <= numTables; i++ {
		ds.makeSet(i)
		trueIdMap[i] = i
	}
	return tableMerger{tables, maxNumRows, ds, trueIdMap}
}

func (tm *tableMerger) getTrueId(tableId int) int {
	return tm.trueIdMap[tm.ds.find(tableId)]
}

func (tm tableMerger) getNumRows(tableId int) int {
	return tm.tables[tableId-1].numRows
}

func (tm *tableMerger) mergeTables(destination, source int) {
	tm.tables[destination-1].numRows += tm.getNumRows(source)
	tm.tables[source-1].numRows = 0
	tm.tables[source-1].symlink = destination
	tm.ds.union(destination, source)
	setId := tm.ds.find(destination)
	tm.trueIdMap[setId] = destination
	if setId == destination {
		delete(tm.trueIdMap, source)
	} else {
		delete(tm.trueIdMap, destination)
	}
}

func (tm *tableMerger) updateMaxNumRows(destination int) {
	if numRows := tm.getNumRows(destination); numRows > tm.maxNumRows {
		tm.maxNumRows = numRows
	}
}

func (tm *tableMerger) runQuery(query mergeQuery) {
	destination := tm.getTrueId(query.destination)
	source := tm.getTrueId(query.source)
	if destination == source {
		fmt.Println(tm.maxNumRows)
		return
	}
	tm.mergeTables(destination, source)
	tm.updateMaxNumRows(destination)
	fmt.Println(tm.maxNumRows)
}

func main() {
	tables, maxNumRows, queries := readInput()
	merger := newTableMerger(tables, maxNumRows)
	for _, query := range queries {
		merger.runQuery(query)
	}
}
