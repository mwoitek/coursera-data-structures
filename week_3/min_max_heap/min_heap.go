package main

import (
	"errors"
	"math"
)

type MinHeap struct {
	arr     []int
	maxSize int
	size    int
}

func NewMinHeap(maxSize int) *MinHeap {
	return &MinHeap{
		arr:     make([]int, 0, maxSize),
		maxSize: maxSize,
		size:    0,
	}
}

func parentIdx(nodeIdx int) int {
	var tmp float64 = float64(nodeIdx + 1)
	tmp = math.Floor(tmp / 2)
	return int(tmp) - 1
}

func (h *MinHeap) siftUp(nodeIdx int) {
	idx := nodeIdx
	var pIdx int
	for idx > 0 {
		pIdx = parentIdx(idx)
		if h.arr[pIdx] <= h.arr[idx] {
			break
		}
		h.arr[pIdx], h.arr[idx] = h.arr[idx], h.arr[pIdx]
		idx = pIdx
	}
}

func leftChildIdx(nodeIdx int) int {
	return 2*nodeIdx + 1
}

func rightChildIdx(nodeIdx int) int {
	return 2 * (nodeIdx + 1)
}

func (h *MinHeap) siftDown(nodeIdx int) {
	minIdx := nodeIdx
	leftIdx := leftChildIdx(nodeIdx)
	if leftIdx < h.size && h.arr[leftIdx] < h.arr[minIdx] {
		minIdx = leftIdx
	}
	rightIdx := rightChildIdx(nodeIdx)
	if rightIdx < h.size && h.arr[rightIdx] < h.arr[minIdx] {
		minIdx = rightIdx
	}
	if minIdx != nodeIdx {
		h.arr[nodeIdx], h.arr[minIdx] = h.arr[minIdx], h.arr[nodeIdx]
		h.siftDown(minIdx)
	}
}

func (h *MinHeap) Push(value int) error {
	if h.size == h.maxSize {
		return errors.New("min heap is full")
	}
	h.arr = append(h.arr, value)
	h.size++
	h.siftUp(h.size - 1)
	return nil
}

func (h *MinHeap) PopMin() (int, error) {
	if h.size == 0 {
		return 0, errors.New("min heap is empty")
	}
	var minValue int = h.arr[0]
	h.arr[0] = h.arr[h.size-1]
	h.size--
	h.siftDown(0)
	return minValue, nil
}

const minusInfinity = math.MinInt

func (h *MinHeap) Pop(nodeIdx int) (int, error) {
	if nodeIdx < 0 || nodeIdx >= h.size {
		return 0, errors.New("min heap is empty or node index is invalid")
	}
	var value int = h.arr[nodeIdx]
	h.arr[nodeIdx] = minusInfinity
	h.siftUp(nodeIdx)
	h.arr[0] = h.arr[h.size-1]
	h.size--
	h.siftDown(0)
	return value, nil
}
