from heap_checker import *

if __name__ == "__main__":
    arr = [19, 14, 28, 15, 16, 7, 27, 15, 21, 21, 5, 2]
    checker = HeapPropertyChecker(arr, HeapType.MAX_HEAP)
    num_edges = checker.count_all_wrong_children()
    print(f"Number of edges: {num_edges}")
    # Number of edges: 5
