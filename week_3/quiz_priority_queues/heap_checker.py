from enum import Enum
from operator import ge
from operator import le

EMPTY = -1


class HeapType(Enum):
    MIN_HEAP = 0
    MAX_HEAP = 1


class HeapPropertyChecker:
    def __init__(self, arr: list[int], type_: HeapType) -> None:
        self.arr = arr
        self.arr_len = len(arr)
        self.cmp_func = le if type_ == HeapType.MIN_HEAP else ge

    @staticmethod
    def idx_left_child(idx: int) -> int:
        return 2 * idx + 1

    @staticmethod
    def idx_right_child(idx: int) -> int:
        return 2 * (idx + 1)

    def is_valid_index(self, idx: int) -> bool:
        return 0 <= idx < self.arr_len

    def val_left_child(self, idx: int) -> int:
        idx_left = HeapPropertyChecker.idx_left_child(idx)
        if self.is_valid_index(idx_left):
            return self.arr[idx_left]
        return EMPTY

    def val_right_child(self, idx: int) -> int:
        idx_right = HeapPropertyChecker.idx_right_child(idx)
        if self.is_valid_index(idx_right):
            return self.arr[idx_right]
        return EMPTY

    def count_wrong_children(self, idx: int) -> int:
        count = 0
        val = self.arr[idx]
        val_left = self.val_left_child(idx)
        val_right = self.val_right_child(idx)
        if val_left != EMPTY:
            count += 0 if self.cmp_func(val, val_left) else 1
        if val_right != EMPTY:
            count += 0 if self.cmp_func(val, val_right) else 1
        return count

    def count_all_wrong_children(self) -> int:
        return sum(
            map(
                lambda idx: self.count_wrong_children(idx),
                range(self.arr_len),
            )
        )
