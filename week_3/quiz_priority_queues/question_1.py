from heap_checker import *

if __name__ == "__main__":
    arr = [5, 4, 11, 14, 7, 14, 18, 11, EMPTY, 7, 7, EMPTY, EMPTY, 12, 7]
    checker = HeapPropertyChecker(arr, HeapType.MIN_HEAP)
    num_edges = checker.count_all_wrong_children()
    print(f"Number of edges: {num_edges}")
    # Number of edges: 4
