class DynamicArray:
    def __init__(self) -> None:
        self.size = 0
        self.capacity = 1

    def push_back(self) -> None:
        if self.size == self.capacity:
            self.capacity *= 2
        self.size += 1

    def pop_back(self) -> None:
        if self.size == 0:
            return
        self.size -= 1


if __name__ == "__main__":
    # Minimum capacity: Every PushBack is followed by a PopBack
    arr_min = DynamicArray()
    for _ in range(24):
        arr_min.push_back()
        arr_min.pop_back()
    print(f"Minimum capacity: {arr_min.capacity}")

    # Maximum capacity: 24 PushBack's followed by 24 PopBack's
    arr_max = DynamicArray()
    for _ in range(24):
        arr_max.push_back()
    for _ in range(24):
        arr_max.pop_back()
    print(f"Maximum capacity: {arr_max.capacity}")
