# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Question 2: Simulations
# ## Imports

# %%
import matplotlib.pyplot as plt
import numpy as np
import question_2
from matplotlib.axes import Axes
from numpy.typing import NDArray

# %matplotlib inline

# %% [markdown]
# ## Setup

# %%
SIMULATION_FUNCS = {
    1: question_2.simulate_case_1,
    2: question_2.simulate_case_2,
    3: question_2.simulate_case_3,
}

# %%
ns = 2 ** np.array(range(2, 12), dtype=np.int_)
ns


# %%
def compute_num_copies(ns: NDArray[np.int_], case: int) -> NDArray[np.int_]:
    simulation_func = np.vectorize(SIMULATION_FUNCS[case])
    return simulation_func(ns)


# %%
def plot_num_copies(
    ns: NDArray[np.int_], num_copies: NDArray[np.int_], case: int
) -> None:
    ax: Axes
    _, ax = plt.subplots(figsize=(10.0, 7.5))  # type: ignore

    ax.plot(ns, num_copies)
    ax.scatter(ns, num_copies)

    ax.set_xlabel("n")
    ax.set_ylabel("Number of copy operations")
    ax.set_title(f"Case {case}")

    plt.show()


# %% [markdown]
# ## Case 1
# PushBack 2 elements, and then alternate $n/2-1$ PushBack and
# PopBack operations.

# %%
num_copies_1 = compute_num_copies(ns, case=1)
num_copies_1

# %%
plot_num_copies(ns, num_copies_1, case=1)

# %% [markdown]
# ## Case 2
# PushBack $n/2$ elements, and then PopBack $n/2$ elements.

# %%
num_copies_2 = compute_num_copies(ns, case=2)
num_copies_2

# %%
plot_num_copies(ns, num_copies_2, case=2)

# %% [markdown]
# ## Case 3
# Let $n$ be a power of 2. Add $n/2$ elements, then alternate $n/4$ times
# between doing a PushBack of an element and a PopBack.

# %%
num_copies_3 = compute_num_copies(ns, case=3)
num_copies_3

# %%
plot_num_copies(ns, num_copies_3, case=3)

# %% [markdown]
# Clearly, this is the **correct answer**.
