class DynamicArray:
    def __init__(self) -> None:
        self.size = 0
        self.capacity = 1
        self.copies = 0

    def push_back(self) -> None:
        if self.size == self.capacity:
            self.capacity *= 2
            self.copies += self.size
        self.size += 1

    def pop_back(self) -> None:
        if self.size == 0:
            return
        self.size -= 1
        if self.size <= self.capacity // 2:
            self.capacity /= 2
            self.copies += self.size


def simulate_case_1(n: int) -> int:
    """PushBack 2 elements, and then alternate n/2-1 PushBack
    and PopBack operations."""
    arr = DynamicArray()
    arr.push_back()
    arr.push_back()
    for _ in range(n // 2 - 1):
        arr.push_back()
        arr.pop_back()
    return arr.copies


def simulate_case_2(n: int) -> int:
    """PushBack n/2 elements, and then PopBack n/2 elements."""
    arr = DynamicArray()
    for _ in range(n // 2):
        arr.push_back()
    for _ in range(n // 2):
        arr.pop_back()
    return arr.copies


def simulate_case_3(n: int) -> int:
    """Let n be a power of 2. Add n/2 elements, then alternate
    n/4 times between doing a PushBack of an element and a
    PopBack."""
    arr = DynamicArray()
    for _ in range(n // 2):
        arr.push_back()
    for _ in range(n // 4):
        arr.push_back()
        arr.pop_back()
    return arr.copies
