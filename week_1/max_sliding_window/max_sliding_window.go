package main

import (
	"container/list"
	"errors"
	"fmt"
)

func main() {
	var numTerms int
	fmt.Scanf("%d", &numTerms)

	var sequence []int = readSequence(numTerms)

	var windowSize int
	fmt.Scanf("%d", &windowSize)

	maxValues := getMaxValues(sequence, windowSize)
	printSlice(maxValues)
}

func readSequence(numTerms int) []int {
	sequence := make([]int, numTerms)
	for i := range sequence {
		fmt.Scanf("%d", &sequence[i])
	}
	return sequence
}

func getMaxValues(sequence []int, windowSize int) []int {
	numTerms := len(sequence)
	maxValues := make([]int, numTerms-windowSize+1)

	d := NewDeque()
	var back int
	var err error

	for i := 0; i < windowSize; i++ {
		for back, err = d.Back(); (err == nil) && (sequence[i] >= sequence[back]); back, err = d.Back() {
			d.PopBack()
		}
		d.PushBack(i)
	}

	idx := 0
	front, err := d.Front()
	maxValues[idx] = sequence[front]
	idx++

	for i := windowSize; i < numTerms; i++ {
		for front, err = d.Front(); (err == nil) && (front <= i-windowSize); front, err = d.Front() {
			d.PopFront()
		}

		for back, err = d.Back(); (err == nil) && (sequence[i] >= sequence[back]); back, err = d.Back() {
			d.PopBack()
		}
		d.PushBack(i)

		front, err = d.Front()
		maxValues[idx] = sequence[front]
		idx++
	}

	return maxValues
}

func printSlice(intSlice []int) {
	lastIdx := len(intSlice) - 1
	for i := 0; i < lastIdx; i++ {
		fmt.Printf("%d ", intSlice[i])
	}
	fmt.Printf("%d\n", intSlice[lastIdx])
}

// Deque implementation based on a doubly linked list

type Deque struct {
	lst *list.List
}

func NewDeque() *Deque {
	return &Deque{lst: list.New()}
}

func (d *Deque) PushFront(value int) {
	d.lst.PushFront(value)
}

func (d *Deque) PushBack(value int) {
	d.lst.PushBack(value)
}

func (d *Deque) PopFront() error {
	elem := d.lst.Front()
	if elem == nil {
		return errors.New("deque is empty")
	}
	d.lst.Remove(elem)
	return nil
}

func (d *Deque) PopBack() error {
	elem := d.lst.Back()
	if elem == nil {
		return errors.New("deque is empty")
	}
	d.lst.Remove(elem)
	return nil
}

func (d *Deque) Front() (int, error) {
	elem := d.lst.Front()
	if elem == nil {
		return -1, errors.New("deque is empty")
	}
	return elem.Value.(int), nil
}

func (d *Deque) Back() (int, error) {
	elem := d.lst.Back()
	if elem == nil {
		return -1, errors.New("deque is empty")
	}
	return elem.Value.(int), nil
}

func (d *Deque) Empty() bool {
	return d.lst.Len() == 0
}
