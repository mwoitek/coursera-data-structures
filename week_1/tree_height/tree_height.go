package main

import (
	"container/list"
	"errors"
	"fmt"
)

func main() {
	myTree := getTree()
	treeHeight := myTree.height()
	fmt.Println(treeHeight)
}

// Very simple tree structure

type node struct {
	children []*node
	depth    int // # edges from node to root
}

func newNode() *node {
	return &node{children: make([]*node, 0), depth: -1}
}

func (n *node) addChild(child *node) {
	n.children = append(n.children, child)
}

type tree struct {
	nodes   []*node
	rootIdx int
}

func newTree(numNodes int) *tree {
	nodes := make([]*node, numNodes)
	for i := range nodes {
		nodes[i] = newNode()
	}
	return &tree{nodes: nodes, rootIdx: -1}
}

func getTree() *tree {
	var numNodes int
	fmt.Scanf("%d", &numNodes)

	myTree := newTree(numNodes)
	var parentIdx int

	for childIdx := 0; childIdx < numNodes; childIdx++ {
		fmt.Scanf("%d", &parentIdx)
		if parentIdx == -1 {
			myTree.rootIdx = childIdx
			myTree.nodes[myTree.rootIdx].depth = 0
		} else {
			myTree.nodes[parentIdx].addChild(myTree.nodes[childIdx])
		}
	}

	return myTree
}

func (t *tree) height() int {
	q := NewQueue()
	q.Enqueue(t.nodes[t.rootIdx])
	var n *node

	for !q.Empty() {
		n, _ = q.Front()
		q.Dequeue()
		for _, c := range n.children {
			q.Enqueue(c)
			c.depth = n.depth + 1
		}
	}

	return n.depth + 1
}

// Queue implementation based on a doubly linked list

type Queue struct {
	lst *list.List
}

func NewQueue() *Queue {
	return &Queue{lst: list.New()}
}

func (q *Queue) Enqueue(value *node) {
	q.lst.PushBack(value)
}

func (q *Queue) Dequeue() error {
	if q.lst.Len() == 0 {
		return errors.New("queue is empty")
	}
	elem := q.lst.Front()
	q.lst.Remove(elem)
	return nil
}

func (q *Queue) Front() (*node, error) {
	if q.lst.Len() == 0 {
		return nil, errors.New("queue is empty")
	}
	return q.lst.Front().Value.(*node), nil
}

func (q *Queue) Empty() bool {
	return q.lst.Len() == 0
}
