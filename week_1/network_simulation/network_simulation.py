from collections import deque


def main():
    ints = input().split(" ")
    ints = [int(x) for x in ints]
    buffer_size, num_packets = ints[0], ints[1]
    input_times = read_times(num_packets)

    times_processed = get_times_processed(input_times, buffer_size)
    print_times_processed(times_processed)


def read_times(num_packets):
    input_times = [None] * num_packets
    for i in range(num_packets):
        ints = input().split(" ")
        ints = [int(x) for x in ints]
        input_times[i] = (ints[0], ints[1])
    return input_times


def get_times_processed(input_times, buffer_size):
    num_packets = len(input_times)
    times_processed = [-1] * num_packets
    finish_times = deque([], buffer_size)

    for i, (time_arrival, time_process) in enumerate(input_times):
        while len(finish_times) > 0 and finish_times[0][0] <= time_arrival:
            tmp = finish_times.popleft()
            finish_time, idx = tmp[0], tmp[1]
            times_processed[idx] = finish_time - input_times[idx][1]

        num_buffered = len(finish_times)
        if num_buffered == buffer_size:
            continue

        if num_buffered > 0:
            finish_time = finish_times[num_buffered - 1][0] + time_process
        else:
            finish_time = time_arrival + time_process
        finish_times.append((finish_time, i))

    while len(finish_times) > 0:
        tmp = finish_times.popleft()
        finish_time, idx = tmp[0], tmp[1]
        times_processed[idx] = finish_time - input_times[idx][1]

    return times_processed


def print_times_processed(times_processed):
    for time in times_processed:
        print(time)


if __name__ == "__main__":
    main()
