package main

import (
	"errors"
	"fmt"
)

func main() {
	var numQueries int
	fmt.Scanf("%d", &numQueries)

	var numInputs int
	var method string
	var value int
	stack := NewMaxStack()
	var maxValue int
	maxValues := make([]int, 0)

	for query := 0; query < numQueries; query++ {
		numInputs, _ = fmt.Scanf("%s %d", &method, &value)
		if numInputs == 2 {
			stack.Push(value)
		} else if method == "pop" {
			stack.Pop()
		} else {
			maxValue, _ = stack.Max()
			maxValues = append(maxValues, maxValue)
		}
	}

	for _, maxValue := range maxValues {
		fmt.Println(maxValue)
	}
}

// Implementation of a singly linked list

type node struct {
	value interface{}
	next  *node
}

type SinglyLinkedList struct {
	head *node
	size int
}

func (l *SinglyLinkedList) PushFront(value interface{}) {
	n := &node{value: value}
	if l.head == nil {
		l.head = n
	} else {
		n.next = l.head
		l.head = n
	}
	l.size++
}

func (l *SinglyLinkedList) PopFront() error {
	if l.head == nil {
		return errors.New("singly linked list is empty")
	}
	l.head = l.head.next
	l.size--
	return nil
}

func (l *SinglyLinkedList) Front() (interface{}, error) {
	if l.head == nil {
		return nil, errors.New("singly linked list is empty")
	}
	return l.head.value, nil
}

func (l *SinglyLinkedList) Empty() bool {
	return l.size == 0
}

// Implementation of a max stack based on a singly linked list

type pair struct {
	value    int
	maxValue int
}

type MaxStack struct {
	list *SinglyLinkedList
}

func NewMaxStack() *MaxStack {
	return &MaxStack{list: &SinglyLinkedList{}}
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func (ms *MaxStack) Push(value int) {
	var maxValue int
	if ms.list.Empty() {
		maxValue = value
	} else {
		front, _ := ms.list.Front()
		maxValue = max(value, front.(pair).maxValue)
	}
	ms.list.PushFront(pair{value: value, maxValue: maxValue})
}

func (ms *MaxStack) Pop() error {
	if ms.list.Empty() {
		return errors.New("max stack is empty")
	}
	ms.list.PopFront()
	return nil
}

func (ms *MaxStack) Max() (int, error) {
	if ms.list.Empty() {
		return -1, errors.New("max stack is empty")
	}
	front, _ := ms.list.Front()
	return front.(pair).maxValue, nil
}
