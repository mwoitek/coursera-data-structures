package main

import (
	"errors"
	"fmt"
)

func main() {
	var inputStr string
	fmt.Scanf("%s", &inputStr)

	answer, idx := isBalanced(inputStr)
	if answer {
		fmt.Println("Success")
	} else {
		fmt.Println(idx)
	}
}

// Simple implementation of a singly linked list
// Only what's needed to implement a stack

type node struct {
	value interface{}
	next  *node
}

type SinglyLinkedList struct {
	head *node
	size int
}

func (l *SinglyLinkedList) PushFront(value interface{}) {
	n := &node{value: value}
	if l.head == nil {
		l.head = n
	} else {
		n.next = l.head
		l.head = n
	}
	l.size++
}

func (l *SinglyLinkedList) PopFront() error {
	if l.head == nil {
		return errors.New("singly linked list is empty")
	}
	l.head = l.head.next
	l.size--
	return nil
}

func (l *SinglyLinkedList) Front() (interface{}, error) {
	if l.head == nil {
		return nil, errors.New("singly linked list is empty")
	}
	return l.head.value, nil
}

func (l *SinglyLinkedList) Back() (interface{}, error) {
	if l.head == nil {
		return nil, errors.New("singly linked list is empty")
	}
	current := l.head
	next := current.next
	for next != nil {
		current = next
		next = current.next
	}
	return current.value, nil
}

func (l *SinglyLinkedList) Size() int {
	return l.size
}

func (l *SinglyLinkedList) Empty() bool {
	return l.size == 0
}

// Implementation of a stack based on a singly linked list
// Only what I'll actually use to solve this problem

type Stack struct {
	list *SinglyLinkedList
}

func NewStack() *Stack {
	return &Stack{list: &SinglyLinkedList{}}
}

func (s *Stack) Push(value interface{}) {
	s.list.PushFront(value)
}

func (s *Stack) Pop() error {
	if s.list.Empty() {
		return errors.New("stack is empty")
	}
	s.list.PopFront()
	return nil
}

func (s *Stack) Top() (interface{}, error) {
	if s.list.Empty() {
		return nil, errors.New("stack is empty")
	}
	value, _ := s.list.Front()
	return value, nil
}

// Non-standard operation, but in this problem it's useful
func (s *Stack) bottom() (interface{}, error) {
	if s.list.Empty() {
		return nil, errors.New("stack is empty")
	}
	value, _ := s.list.Back()
	return value, nil
}

func (s *Stack) Size() int {
	return s.list.Size()
}

func (s *Stack) Empty() bool {
	return s.list.Empty()
}

// Solution to the problem

func isOpeningBracket(char rune) bool {
	return char == '(' || char == '[' || char == '{'
}

func isClosingBracket(char rune) bool {
	return char == ')' || char == ']' || char == '}'
}

func bracketsMatch(opening, closing rune) bool {
	return (opening == '(' && closing == ')') ||
		(opening == '[' && closing == ']') ||
		(opening == '{' && closing == '}')
}

type pair struct {
	bracket rune
	idx     int
}

func isBalanced(str string) (bool, int) {
	s := NewStack()

	for i, char := range str {
		if isOpeningBracket(char) {
			s.Push(pair{bracket: char, idx: i + 1})
		} else if isClosingBracket(char) {
			if s.Empty() {
				return false, i + 1
			}
			top, _ := s.Top()
			s.Pop()
			if !bracketsMatch(top.(pair).bracket, char) {
				return false, i + 1
			}
		}
	}

	if s.Empty() {
		return true, -1
	}
	bottom, _ := s.bottom()
	return false, bottom.(pair).idx
}
