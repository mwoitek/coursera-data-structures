package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func readInput() (uint, []string) {
	var inputInt int
	fmt.Scanf("%d", &inputInt)
	m := uint(inputInt)
	fmt.Scanf("%d", &inputInt)
	queries := make([]string, inputInt)
	reader := bufio.NewReader(os.Stdin)
	var inputStr string
	for i := range queries {
		inputStr, _ = reader.ReadString('\n')
		inputStr = strings.TrimSuffix(inputStr, "\n")
		queries[i] = inputStr
	}
	return m, queries
}

type hashSet struct {
	arr []*list.List
	m   uint
}

func newHashSet(m uint) *hashSet {
	arr := make([]*list.List, m)
	for i := range arr {
		arr[i] = list.New()
	}
	return &hashSet{arr: arr, m: m}
}

func polyHash(s string, x, p, m uint) uint {
	var hash uint
	hash = 0
	chars := []rune(s)
	var r uint
	for i := len(chars) - 1; i >= 0; i-- {
		r = (hash * x) % p
		hash = (r + uint(chars[i])) % p
	}
	hash = hash % m
	return hash
}

func (hs *hashSet) getChain(s string) *list.List {
	idx := polyHash(s, 263, 1000000007, hs.m)
	return hs.arr[idx]
}

func (hs *hashSet) add(s string) {
	chain := hs.getChain(s)
	for elem := chain.Front(); elem != nil; elem = elem.Next() {
		if elem.Value.(string) == s {
			return
		}
	}
	chain.PushBack(s)
}

func (hs *hashSet) remove(s string) {
	chain := hs.getChain(s)
	var elemToRemove *list.Element
	elemToRemove = nil
	for elem := chain.Front(); elem != nil; elem = elem.Next() {
		if elem.Value.(string) == s {
			elemToRemove = elem
			break
		}
	}
	if elemToRemove != nil {
		chain.Remove(elemToRemove)
	}
}

func (hs *hashSet) contains(s string) bool {
	chain := hs.getChain(s)
	for elem := chain.Front(); elem != nil; elem = elem.Next() {
		if elem.Value.(string) == s {
			return true
		}
	}
	return false
}

func (hs *hashSet) check(i int) {
	chain := hs.arr[i]
	var strArr []string
	for elem := chain.Back(); elem != nil; elem = elem.Prev() {
		strArr = append(strArr, elem.Value.(string))
	}
	if len(strArr) > 0 {
		fmt.Println(strings.Join(strArr, " "))
	} else {
		fmt.Println()
	}
}

func (hs *hashSet) runQuery(query string) {
	queryParts := strings.Fields(query)
	queryType := queryParts[0]
	switch queryType {
	case "add":
		hs.add(queryParts[1])
	case "del":
		hs.remove(queryParts[1])
	case "find":
		if hs.contains(queryParts[1]) {
			fmt.Println("yes")
		} else {
			fmt.Println("no")
		}
	case "check":
		i, _ := strconv.Atoi(queryParts[1])
		hs.check(i)
	}
}

func main() {
	m, queries := readInput()
	hs := newHashSet(m)
	for _, query := range queries {
		hs.runQuery(query)
	}
}
