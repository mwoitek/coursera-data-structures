package main

import (
	"fmt"
	"strconv"
	"strings"
)

const largePrime = 99194853094755497

func readInput() (string, string) {
	var pattern string
	var text string
	fmt.Scanf("%s", &pattern)
	fmt.Scanf("%s", &text)
	return pattern, text
}

func modMultiply(x, y, m int) int {
	result := 0
	x %= m
	y %= m
	for x > 0 {
		if x%2 != 0 {
			result = (result + y) % m
		}
		y = (2 * y) % m
		x /= 2
	}
	return result
}

func polyHash(s string, x, p int) int {
	hash := 0
	chars := []rune(s)
	for i := len(s) - 1; i >= 0; i-- {
		hash = (modMultiply(x, hash, p) + int(chars[i])) % p
	}
	return hash
}

func modPow(b, e, m int) int {
	result := 1
	b %= m
	for e > 0 {
		if e%2 != 0 {
			result = modMultiply(result, b, m)
		}
		b = modMultiply(b, b, m)
		e /= 2
	}
	return result
}

func precomputeHashes(text string, patternLen, x, p int) []int {
	textLen := len(text)
	hashes := make([]int, textLen-patternLen+1)
	hashes[textLen-patternLen] = polyHash(text[textLen-patternLen:textLen], x, p)
	chars := []rune(text)
	power := modPow(x, patternLen, p)
	var prod1 int
	var prod2 int
	for i := textLen - patternLen - 1; i >= 0; i-- {
		prod1 = modMultiply(x, hashes[i+1], p)
		prod2 = p - modMultiply(int(chars[i+patternLen]), power, p)
		hashes[i] = ((prod1+prod2+int(chars[i]))%p + p) % p
	}
	return hashes
}

func rabinKarp(text, pattern string) []int {
	var idxs []int
	x := 2
	patternLen := len(pattern)
	patternHash := polyHash(pattern, x, largePrime)
	substringHashes := precomputeHashes(text, patternLen, x, largePrime)
	for idx := 0; idx <= len(text)-patternLen; idx++ {
		if substringHashes[idx] != patternHash {
			continue
		}
		if text[idx:idx+patternLen] == pattern {
			idxs = append(idxs, idx)
		}
	}
	return idxs
}

func main() {
	pattern, text := readInput()
	idxs := rabinKarp(text, pattern)
	var idxStr string
	var builder strings.Builder
	for _, idx := range idxs {
		idxStr = strconv.Itoa(idx)
		builder.WriteString(idxStr + " ")
	}
	out := strings.TrimRight(builder.String(), " ")
	fmt.Println(out)
}
