// This algorithm is correct. However, it is not fast enough to pass this
// assignment!!!

#include <array>
#include <iostream>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

using std::string;
using std::vector;

const string ALPHABET = "abcdefghijklmnopqrstuvwxyz";
const unsigned ALPHABET_SIZE = 26;

// Reading input

struct InputTriple {
  unsigned k = 0U;
  string text;
  string pattern;
};

auto read_input() -> vector<InputTriple> {
  vector<InputTriple> triples;
  InputTriple triple;
  while (std::cin >> triple.k >> triple.text >> triple.pattern) {
    triples.push_back(triple);
  }
  return triples;
}

// The following code corresponds to an implementation of the
// algorithm described in this paper:

// Simple and efficient string matching with k mismatches
// R. Grossi, F. Luccio

// https://www.sciencedirect.com/science/article/pii/0020019089901889

auto letter_to_index(const char letter) -> unsigned {
  return static_cast<unsigned>(letter - ALPHABET[0]);
}

auto hamming_distance(const string& str_1, const string& str_2) -> unsigned {
  unsigned distance = 0U;
  for (unsigned i = 0U; i < str_1.length(); ++i) {
    if (str_1[i] != str_2[i]) {
      ++distance;
    }
  }
  return distance;
}

auto approximate_pattern_matching(const string& text, const string& pattern,
                                  const unsigned k)  // NOLINT
    -> vector<unsigned> {
  vector<unsigned> indexes;

  std::array<int, ALPHABET_SIZE> counter{};
  for (const char letter : pattern) {
    ++counter.at(letter_to_index(letter));
  }

  const unsigned pattern_len = pattern.length();
  std::queue<char> q;  // NOLINT
  unsigned num_mismatches = 0U;

  for (unsigned i = 0U; i < text.length(); ++i) {
    if (q.size() == pattern_len) {
      const char q_letter = q.front();
      q.pop();
      const unsigned letter_idx = letter_to_index(q_letter);
      if (counter.at(letter_idx) < 0) {
        --num_mismatches;
      }
      ++counter.at(letter_idx);
    }

    q.push(text[i]);
    const unsigned letter_idx = letter_to_index(text[i]);
    --counter.at(letter_idx);
    if (counter.at(letter_idx) < 0) {
      ++num_mismatches;
    }

    while (num_mismatches > k) {
      const char q_letter = q.front();
      q.pop();
      const unsigned letter_idx = letter_to_index(q_letter);
      if (counter.at(letter_idx) < 0) {
        --num_mismatches;
      }
      ++counter.at(letter_idx);
    }

    if (q.size() == pattern_len &&
        hamming_distance(text.substr(i - pattern_len + 1U, pattern_len),
                         pattern) <= k) {
      indexes.push_back(i - pattern_len + 1U);
    }
  }

  return indexes;
}

void print_indexes(const vector<unsigned>& indexes) {
  std::stringstream out_stream;
  out_stream << indexes.size() << ' ';
  for (const unsigned index : indexes) {
    out_stream << index << ' ';
  }
  string out = out_stream.str();
  out.pop_back();
  std::cout << out << '\n';
}

auto main() -> int {
  std::ios_base::sync_with_stdio(false);
  const vector<InputTriple> input = read_input();
  for (const InputTriple& triple : input) {
    const vector<unsigned> indexes =
        approximate_pattern_matching(triple.text, triple.pattern, triple.k);
    print_indexes(indexes);
  }
  return 0;
}
