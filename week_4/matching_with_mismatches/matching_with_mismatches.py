"""
This code corresponds to an implementation of Algorithm A3 described in the
following paper:
"Simple and efficient string matching with k mismatches"
R. Grossi, F. Luccio
"""

# This algorithm is correct. However, it is not fast enough to pass this
# assignment!!!

from queue import Queue
from string import ascii_lowercase as ALPHABET
from sys import stdin
from typing import List


def test_hamming_dist(x: str, y: str, k: int) -> bool:
    hamming_distance = 0
    i = 0
    length = len(x)
    while hamming_distance <= k and i < length:
        hamming_distance += 1 if x[i] != y[i] else 0
        i += 1
    return hamming_distance <= k


def approximate_pattern_matching(text: str, pattern: str, k: int) -> List[int]:
    indexes: List[int] = []

    counter = {letter: 0 for letter in ALPHABET}
    for letter in pattern:
        counter[letter] += 1

    pattern_len = len(pattern)
    q = Queue(maxsize=pattern_len)
    num_mismatches = 0

    for i, letter in enumerate(text):
        if q.full():
            q_letter = q.get()
            if counter[q_letter] < 0:
                num_mismatches -= 1
            counter[q_letter] += 1

        q.put(letter)
        counter[letter] -= 1
        if counter[letter] < 0:
            num_mismatches += 1

        while num_mismatches > k:
            q_letter = q.get()
            if counter[q_letter] < 0:
                num_mismatches -= 1
            counter[q_letter] += 1

        if q.full() and test_hamming_dist(text[i - pattern_len + 1 : i + 1], pattern, k):
            indexes.append(i - pattern_len + 1)

    return indexes


def print_indexes(indexes: List[int]) -> None:
    print(len(indexes), end=" ")
    print(" ".join(map(str, indexes)))


if __name__ == "__main__":
    for line in stdin:
        line_parts = line.strip().split(" ")
        k = int(line_parts[0])
        text, pattern = line_parts[1:]
        indexes = approximate_pattern_matching(text, pattern, k)
        print_indexes(indexes)
