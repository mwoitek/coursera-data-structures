from typing import Callable
from typing import List
from typing import Tuple

m1 = 1000000007
m2 = 1000000009
x = 30011


def read_input() -> Tuple[str, List[List[int]], int]:
    string = input()
    num_queries = int(input())
    queries = [[]] * num_queries
    for i in range(num_queries):
        queries[i] = list(map(int, input().split(" ")))
    max_len = max(map(lambda i: queries[i][2], range(num_queries)))
    return string, queries, max_len


def prefix_hashes(string: str, m: int) -> List[int]:
    hashes = [0] * (len(string) + 1)
    for i, char in enumerate(map(ord, string)):
        hashes[i + 1] = (hashes[i] * x + char) % m
    return hashes


def powers_of_x(max_len: int, m: int) -> List[int]:
    powers = [1] * (max_len + 1)
    for i in range(max_len):
        powers[i + 1] = (powers[i] * x) % m
    return powers


def substring_hash_calculator(
    string: str, max_len: int, m: int
) -> Callable[[int, int], int]:
    hashes = prefix_hashes(string, m)
    powers = powers_of_x(max_len, m)

    def substring_hash(idx: int, length: int) -> int:
        return (hashes[idx + length] - hashes[idx] * powers[length]) % m

    return substring_hash


if __name__ == "__main__":
    string, queries, max_len = read_input()
    substring_hash_1 = substring_hash_calculator(string, max_len, m1)
    substring_hash_2 = substring_hash_calculator(string, max_len, m2)
    for a, b, l in queries:
        test_1 = substring_hash_1(a, l) == substring_hash_1(b, l)
        if test_1 and substring_hash_2(a, l) == substring_hash_2(b, l):
            print("Yes")
        else:
            print("No")
