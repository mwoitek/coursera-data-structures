package main

import "fmt"

const (
	m1 = 1000000007
	m2 = 1000000009
	x  = 30011
)

// This code works. However, it is not fast enough to pass test #7.
// I cannot understand why this is the case. I've translated my Go
// code into Python, and the Python implementation passes all tests
// comfortably.

func readInput() (string, [][3]int, int) {
	var inputStr string
	fmt.Scanf("%s", &inputStr)
	var numQueries int
	fmt.Scanf("%d", &numQueries)
	queries := make([][3]int, numQueries)
	maxLength := -1
	for i := 0; i < numQueries; i++ {
		fmt.Scanf("%d %d %d", &queries[i][0], &queries[i][1], &queries[i][2])
		if queries[i][2] > maxLength {
			maxLength = queries[i][2]
		}
	}
	return inputStr, queries, maxLength
}

func prefixHashes(s string, m int) []int {
	hashes := make([]int, len(s)+1)
	hashes[0] = 0
	for i, r := range s {
		hashes[i+1] = (hashes[i]*x + int(r-'0')) % m
	}
	return hashes
}

func powersOfX(maxLength, m int) []int {
	powers := make([]int, maxLength+1)
	powers[0] = 1
	for i := 0; i < maxLength; i++ {
		powers[i+1] = (powers[i] * x) % m
	}
	return powers
}

func substringHashCalculator(s string, maxLength, m int) func(int, int) int {
	hashes := prefixHashes(s, m)
	powers := powersOfX(maxLength, m)
	return func(idx, length int) int {
		prod := m - (hashes[idx]*powers[length])%m
		return (hashes[idx+length] + prod) % m
	}
}

func main() {
	inputStr, queries, maxLength := readInput()
	substringHash1 := substringHashCalculator(inputStr, maxLength, m1)
	substringHash2 := substringHashCalculator(inputStr, maxLength, m2)
	var a, b, l int
	for i := range queries {
		a = queries[i][0]
		b = queries[i][1]
		l = queries[i][2]
		if substringHash1(a, l) == substringHash1(b, l) && substringHash2(a, l) == substringHash2(b, l) {
			fmt.Println("Yes")
		} else {
			fmt.Println("No")
		}
	}
}
