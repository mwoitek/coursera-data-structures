from sys import stdin
from typing import Dict
from typing import List
from typing import Tuple


def read_input() -> List[List[str]]:
    strings = []
    for line in stdin.readlines():
        strings.append(line.strip().split(" "))
    return strings


class HashCalculator:
    def __init__(self, string: str, substr_len: int, x: int, m: int) -> None:
        self.string = string
        self.substr_len = substr_len
        self.x = x
        self.m = m
        self.hashes = []

    def polynomial_hash(self, first_idx: int, last_idx: int) -> int:
        hash = 0
        chars = list(map(ord, self.string[first_idx:last_idx]))
        for char in reversed(chars):
            hash = (hash * self.x + char) % self.m
        return hash

    def substring_hashes(self) -> List[int]:
        if len(self.hashes) > 0:
            return self.hashes
        str_len = len(self.string)
        self.hashes = [0] * (str_len - self.substr_len + 1)
        self.hashes[-1] = self.polynomial_hash(str_len - self.substr_len, str_len)
        chars = list(map(ord, self.string))
        power = pow(self.x, self.substr_len, self.m)
        for i in range(str_len - self.substr_len - 1, -1, -1):
            prod1 = self.hashes[i + 1] * self.x
            prod2 = chars[i + self.substr_len] * power
            self.hashes[i] = (chars[i] + prod1 - prod2) % self.m
        return self.hashes


class HashesAggregator:
    def __init__(self, string: str, substr_len: int, x: int, m1: int, m2: int) -> None:
        self.calc1 = HashCalculator(string, substr_len, x, m1)
        self.calc2 = HashCalculator(string, substr_len, x, m2)
        _ = self.calc1.substring_hashes()
        _ = self.calc2.substring_hashes()
        self.hashes_dict = dict()

    def pair_of_hashes(self, idx: int) -> Tuple[int, int]:
        hash1 = self.calc1.hashes[idx]
        hash2 = self.calc2.hashes[idx]
        return hash1, hash2

    def dict_of_hashes(self) -> Dict[Tuple[int, int], int]:
        if len(self.hashes_dict) > 0:
            return self.hashes_dict
        num_hashes = len(self.calc1.hashes)
        for idx in range(num_hashes):
            self.hashes_dict[self.pair_of_hashes(idx)] = idx
        return self.hashes_dict


class CommonSubstringFinder:
    def __init__(self, string1: str, string2: str, x: int, m1: int, m2: int) -> None:
        self.string1 = string1
        self.string2 = string2
        self.x = x
        self.m1 = m1
        self.m2 = m2

    def have_common_substring(self, substr_len: int) -> Tuple[bool, int, int]:
        aggregator1 = HashesAggregator(self.string1, substr_len, self.x, self.m1, self.m2)
        aggregator2 = HashesAggregator(self.string2, substr_len, self.x, self.m1, self.m2)
        _ = aggregator1.dict_of_hashes()
        num_hashes2 = len(aggregator2.calc1.hashes)
        for idx2 in range(num_hashes2):
            pair2 = aggregator2.pair_of_hashes(idx2)
            if pair2 in aggregator1.hashes_dict:
                idx1 = aggregator1.hashes_dict[pair2]
                return True, idx1, idx2
        return False, -1, -1

    def longest_common_substring(self) -> Tuple[int, int, int]:
        idx1, idx2, substr_len = -1, -1, 0
        lo, hi = 0, min(len(self.string1), len(self.string2))
        while lo <= hi:
            mid = (lo + hi) // 2
            have_common, i1, i2 = self.have_common_substring(mid)
            if have_common:
                idx1, idx2, substr_len = i1, i2, mid
                lo = mid + 1
            else:
                hi = mid - 1
        return idx1, idx2, substr_len


if __name__ == "__main__":
    x = 514229
    m1 = 9999999967
    m2 = 10000000019
    strings = read_input()
    for string1, string2 in strings:
        finder = CommonSubstringFinder(string1, string2, x, m1, m2)
        idx1, idx2, substr_len = finder.longest_common_substring()
        print(idx1, idx2, substr_len)
