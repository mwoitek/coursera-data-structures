package main

import (
	"bufio"
	"container/list"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func readInput() []string {
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	input = strings.TrimSuffix(input, "\n")
	numQueries, _ := strconv.Atoi(input)
	queries := make([]string, numQueries)
	for i := range queries {
		input, _ = reader.ReadString('\n')
		input = strings.TrimSuffix(input, "\n")
		queries[i] = input
	}
	return queries
}

type phoneBookPair struct {
	phoneNumber int
	name        string
}

type phoneBook struct {
	arr []*list.List
}

func newPhoneBook(cardinality int) *phoneBook {
	arr := make([]*list.List, cardinality)
	for i := range arr {
		arr[i] = list.New()
	}
	return &phoneBook{arr: arr}
}

func (pb *phoneBook) cardinality() int {
	return len(pb.arr)
}

func hashFunc(a, b, x, p, m int) int {
	l := a*x + b
	r := l % p
	return r % m
}

func (pb *phoneBook) getChain(phoneNumber int) *list.List {
	idx := hashFunc(34, 2, phoneNumber, 10000019, pb.cardinality())
	return pb.arr[idx]
}

func (pb *phoneBook) set(phoneNumber int, name string) {
	chain := pb.getChain(phoneNumber)
	for elem := chain.Front(); elem != nil; elem = elem.Next() {
		pbp := elem.Value.(*phoneBookPair)
		if pbp.phoneNumber == phoneNumber {
			pbp.name = name
			return
		}
	}
	chain.PushBack(&phoneBookPair{phoneNumber: phoneNumber, name: name})
}

func (pb *phoneBook) remove(phoneNumber int) {
	chain := pb.getChain(phoneNumber)
	var elemToRemove *list.Element
	elemToRemove = nil
	for elem := chain.Front(); elem != nil; elem = elem.Next() {
		pbp := elem.Value.(*phoneBookPair)
		if pbp.phoneNumber == phoneNumber {
			elemToRemove = elem
			break
		}
	}
	if elemToRemove != nil {
		chain.Remove(elemToRemove)
	}
}

func (pb *phoneBook) get(phoneNumber int) (string, error) {
	chain := pb.getChain(phoneNumber)
	for elem := chain.Front(); elem != nil; elem = elem.Next() {
		pbp := elem.Value.(*phoneBookPair)
		if pbp.phoneNumber == phoneNumber {
			return pbp.name, nil
		}
	}
	return "", errors.New("not found")
}

func (pb *phoneBook) runQuery(query string) {
	queryParts := strings.Fields(query)
	queryType := queryParts[0]
	phoneNumber, _ := strconv.Atoi(queryParts[1])
	switch queryType {
	case "add":
		name := queryParts[2]
		pb.set(phoneNumber, name)
	case "del":
		pb.remove(phoneNumber)
	case "find":
		name, err := pb.get(phoneNumber)
		if err == nil {
			fmt.Println(name)
		} else {
			fmt.Println("not found")
		}
	}
}

func main() {
	queries := readInput()
	pb := newPhoneBook(3*10 ^ 4)
	for _, query := range queries {
		pb.runQuery(query)
	}
}
